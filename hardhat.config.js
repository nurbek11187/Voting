require("dotenv").config();
require("@nomiclabs/hardhat-web3");
require("@nomiclabs/hardhat-web3");
Web3 = require('web3')
const jsonabi = require('./artifacts/contracts/Voting.sol/Voting.json')
const Tx = require('ethereumjs-tx').Transaction
const { task } = require("hardhat/config");
require("@nomiclabs/hardhat-etherscan");
require("@nomiclabs/hardhat-waffle");
require("hardhat-gas-reporter");
require("solidity-coverage");
const Contract = require("web3-eth-contract");

var web3 = new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io/v3/e3cee0bae01f47a8a18e4debf2cd0063'));
// создание контракта на web3.js 

var contract = new web3.eth.Contract(jsonabi.abi, "0x2B81477A8FB6a954B62d687F9D5b35B999E3d67E");

//tasks
task('create_vote', "test desctiption")
  .setAction(async () => {
    contractMethod = contract.methods.createVoting(
      ["0x4c54a50B8F9D4f2EBfa187f4633A581440096678", "0xb62031Fc947998cB9B55f170566D521E7EdEEF40"],
      ["123", "123"]
    )
    account = web3.utils.toChecksumAddress("0x479268d467648bb567db1b8fd3b7549498db71fc");
    const transactionCount = await web3.eth.getTransactionCount(account, 'latest');


    const txObject = await web3.eth.accounts.signTransaction ({
      nonce: transactionCount,
      gas: '800000',
      to: '0x2B81477A8FB6a954B62d687F9D5b35B999E3d67E',
      gasPrice: web3.utils.toWei('10', 'gwei'),
      data: contractMethod.encodeABI()
    },
    `${process.env.PRIVATE_KEY}`
    )
    // Sign the transaction
    const createReceipt = await web3.eth.sendSignedTransaction(
      txObject.rawTransaction
   );
   console.log(
      `Transaction successful with hash: ${createReceipt.transactionHash}`
   );
})


task('vote', "vote for candidate")
  .setAction(async () => {

    contractMethod = contract.methods.vote(0,0)
    account = '0x479268d467648bb567db1b8fd3b7549498db71fc';
    const transactionCount = await web3.eth.getTransactionCount(account, 'latest');


    const txObject = await web3.eth.accounts.signTransaction ({
      nonce: transactionCount,
      gas: '800000',
      to: '0x2B81477A8FB6a954B62d687F9D5b35B999E3d67E',
      value: web3.utils.toWei('0.01','ether'),
      gasPrice: web3.utils.toWei('10', 'gwei'),
      data: contractMethod.encodeABI()
    },
    `${process.env.PRIVATE_KEY}`
    )
    const createReceipt = await web3.eth.sendSignedTransaction(
      txObject.rawTransaction
   );
   console.log(
      `Transaction successful with hash: ${createReceipt.transactionHash}`
   );
})
//Этот таск работает, но на контракте выйдет ошибка что (Fail with error 'Not time to end vote') если подождете 3 дня то можно закрыть
task('endvote', "end voting")
  .setAction(async () => {
    contractMethod = contract.methods.endVote(0)
    account = '0x479268d467648bb567db1b8fd3b7549498db71fc';
    const transactionCount = await web3.eth.getTransactionCount(account, 'latest');


    const txObject = await web3.eth.accounts.signTransaction ({
      nonce: transactionCount,
      gas: '800000',
      to: '0x2B81477A8FB6a954B62d687F9D5b35B999E3d67E',
      gasPrice: web3.utils.toWei('10', 'gwei'),
      data: contractMethod.encodeABI()
    },
    `${process.env.PRIVATE_KEY}`
    )
    const createReceipt = await web3.eth.sendSignedTransaction(
      txObject.rawTransaction
   );
   console.log(
      `Transaction successful with hash: ${createReceipt.transactionHash}`
   );
})


module.exports = {
  solidity: "0.8.0",
  networks: {
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${process.env.ALCHEMY_API_KEY}`,
      accounts: [`${process.env.PRIVATE_KEY}`]
    }
  },
  hardhat:{
    chainId: 1337
  },
  etherscan: {
    apiKey: `${process.env.ETHERSCAN_KEY}`
  }

};
