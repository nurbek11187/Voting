//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

contract Voting{
    uint constant minEth = 10000000000000000; // 0,01 ETH
    uint constant adminFee = 1000; // 1000 = 10%, 10000 = 100%
    
    struct Candidate{
        uint256 id;
        address payable wallet;
        string name;
        uint totalVotes;
    }

    enum Status {
        Pending,
        Active,
        Ended
    }

    struct VotingStruct{
        uint startTime; // начало голосования
        uint endTime; //конец голосования
        uint winner;
        Status status;
    }

    address public owner;

    uint public votingsCount;
    mapping(uint => mapping(uint => Candidate)) public candidates; // массив кандидаты на голосование
    mapping(uint => VotingStruct) public votings;

    mapping(uint => mapping(address => bool)) private voters; // массив проголосовавших адресов
    mapping(uint => address[]) private votersArray; // массив проголосовавших адресов
    mapping(uint => uint) private _candidatesCount;
    // VotingStruct private voting;

    constructor ()  {
       owner = msg.sender;
   }
    modifier onlyOwner{
        require(msg.sender == owner, "Sorry, you are not owner");
        _;
    }

    function vote (uint votingId, uint candidateId) external payable{
        require(!voters[votingId][msg.sender], "You have already voted");
        require(msg.value == minEth, "Amount of Etherium must equal 0.01 Etherium");
        require(candidateId >= 0 && candidateId <= _candidatesCount[votingId], "Not candidate with that id");
        votersArray[votingId].push(msg.sender); // Если у данного юзера 0 пожертвовании, то добавить его в массив с юзерами
        voters[votingId][msg.sender] = true;
        candidates[votingId][candidateId].totalVotes++;
    }

    function endVote(uint votingId) external {
        require(votings[votingId].endTime != 0, "No vote currently exists"); //Спросить Алихана. Алих: Можно
        require(block.timestamp >= votings[votingId].endTime, "Not time to end vote");
        
        uint maxVotes;
        uint winnerId;
        for(uint i = 1; i < _candidatesCount[votingId]; i++) {
            if(candidates[votingId][i].totalVotes > maxVotes) {
                winnerId = i;
            }
        }

        candidates[votingId][winnerId].wallet.transfer((address(this).balance * (10000 - adminFee)) / 10000);
        votings[votingId].winner = winnerId;
        votings[votingId].status = Status.Ended;
    }

    function withdrawCollectedFee() external onlyOwner {
        payable(owner).transfer(address(this).balance);
    }

    function createVoting(address[] calldata wallets , string[] calldata names) external onlyOwner{
        require(wallets.length == names.length, "Candidate wallets and names array size is different");
        votings[votingsCount]= VotingStruct({
            startTime: ( block.timestamp),
            endTime: ( block.timestamp + 3 days),
            winner: 0,
            status: Status.Active
        });

        for(uint i = 0; i < wallets.length; i++) {
            addCandidate(votingsCount, wallets[i], names[i]);
        }

        votingsCount++;
    }

    function addCandidate(uint votingId, address wallet, string calldata new_name) public {
        _candidatesCount[votingId]++;
        uint candidatesCount = _candidatesCount[votingId];
        candidates[votingId][candidatesCount] = Candidate(candidatesCount, payable(wallet), new_name, 0 );
    }

    // эта функция список всех голосовавших 
    function getVotersArray(uint votingId) external view returns(address[] memory) {
        return votersArray[votingId];
    }


    
    function getVotingInformations() public view returns(uint[] memory, uint[] memory, uint[] memory){
        uint[] memory startTime = new uint[](votingsCount);
        uint[] memory endTime = new uint[](votingsCount);
        uint[] memory winner = new uint[](votingsCount);

        
        for (uint i = 0; i < votingsCount; i ++){
            VotingStruct storage voting = votings[i];
            startTime[i] = voting.startTime;
            endTime[i] = voting.endTime;
            winner[i] = voting.winner; 
        }
        return (startTime, endTime, winner);
        
    }
    function getVotingInformation(uint votingId) public view returns(uint, uint, uint, Status){
        return (votings[votingId].startTime, votings[votingId].endTime, votings[votingId].winner, votings[votingId].status);
    }

}